<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/', 'ShopController@getProducts');

Route::post('select_ajax_items', ['as'=>'select_ajax_items','uses'=>'ShopController@ajaxFilter']);


Route::get('/admin', function () {
    return view('auth.login');
});

Auth::routes();


Route::get('/unverified', 'HomeController@unverified')->name('unverified');


Route::group(['middleware' => 'verified'], function () {



    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/restore_product/{id}', 'HomeController@restoreproduct');

    Route::resource('products', 'ProductController');
    Route::resource('brands', 'BrandController');
    Route::resource('processors', 'ProcessorController');
    Route::resource('screens', 'ScreenController');
    Route::resource('prices', 'PriceController');



});

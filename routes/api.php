<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('/login', 'API\AuthController@postLogin');
Route::post('/signup', 'API\AuthController@signUp');
Route::post('/test', 'API\AuthController@test');

Route::group(['middleware' => 'auth:api'], function () {
    Route::get('/user', function (Request $request)    {
        return $request->user();
    });

    Route::get('/profile', 'API\ProfileController@getProfile');
    Route::get('/home_data', 'API\GetHomeController@getHomeData');
    Route::get('/bookings', 'API\GetBookingsController@getBookings');
    Route::get('/ongoing', 'API\GetBookingsController@getOngoingBookings');
    Route::get('/completed', 'API\GetBookingsController@getCompletedBookings');
    Route::get('/completed_km_rate', 'API\GetBookingsController@getCompletedBookingsKmRate');
    Route::get('/vehicles', 'API\GetVehicleController@getVehicles');
    Route::get('/vehicle_models', 'API\GetVehicleController@getVehicleModels');
    Route::get('/vehicle_dates', 'API\GetVehicleController@getVehicleDates');
    Route::get('/vehicle_date_expiry', 'API\GetVehicleController@getVehicleDateExpiry');

    Route::post('/change_status', 'API\GetHomeController@changeStatus');
    Route::post('/trip_accepted', 'API\GetBookingsController@tripAccepted');
    Route::post('/trip_rejected', 'API\GetBookingsController@tripRejected');
    Route::post('/end_trip', 'API\GetBookingsController@endTrip');
    Route::post('/start_trip', 'API\GetBookingsController@startTrip');
    Route::post('/edit_profile', 'API\ProfileController@editProfile');
    Route::post('/edit_password', 'API\ProfileController@editPassword');
    Route::post('/edit_profile_pic', 'API\ProfileController@editProfilePic');
    Route::post('/edit_licence_pic', 'API\ProfileController@editLicencePic');
    Route::post('/add_vehicles', 'API\GetVehicleController@addVehicles');
    Route::post('/edit_vehicles', 'API\GetVehicleController@editVehicles');
});

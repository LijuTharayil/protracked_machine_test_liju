@extends('layouts.test-admin-app')

@section('title', 'Edit Prices')

@section('links')

    <link href="{{asset('assets/plugins/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css" />


@endsection


@section('content')

    @include('admin-includes.top-bar')

    @include('admin-includes.left-side-bar')


    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container-fluid">

                <div class="row">
                    <div class="col-xl-12">
                        <div class="card-box">
                            <div class="dropdown pull-right">
                                <a href="{{ route('prices.index') }}" class="btn btn-info waves-effect w-md waves-light m-b-5">Prices</a>
                            </div>
                            <h4 class="header-title m-t-0 m-b-30">Edit Prices Details</h4>

                            @include ('flash.message')
                            @include ('errors.list')

                            {{ Form::model($price, array('route' => array('prices.update', $price->id), 'method' => 'PUT', 'data-parsley-validate','novalidate')) }}



                            <div class="form-group row">

                                <div class="col-12 col-md-3">

                                    <label for="from">Price Range From ₹</label>
                                    <input type="text"  name="from" parsley-trigger="change"
                                           placeholder="" class="form-control" id="from" value="{{ $price->from }}">
                                </div>

                                <div class="col-12 col-md-3">

                                    <label for="to">Price Range To ₹</label>
                                    <input type="text"  name="to" parsley-trigger="change"
                                           placeholder="" class="form-control" id="to" value="{{ $price->to }}">
                                </div>
                                <div class="col-12 col-md-3">

                                    <label for="step">Price Range Step (Range Scale)</label>
                                    <input type="text"  name="step" parsley-trigger="change"
                                           placeholder="" class="form-control" id="step" value="{{ $price->step }}" >
                                </div>
                                <div class="col-12 col-md-3">
                                    <label for="branch_id">Status</label>
                                    <select id="status" class="form-control select2" name="status">
                                        <option value="1"  @php if($price->status == 1) { @endphp selected @php } else {} @endphp >Active</option>
                                        <option value="0" @php if($price->status == 0) { @endphp selected @php } else {} @endphp >In Active</option>


                                    </select>

                                </div>


                            </div>



                            <div class="form-group text-right m-b-0">
                                <button class="btn btn-primary waves-effect waves-light" type="submit">
                                    Submit
                                </button>
                                <button type="reset" class="btn btn-secondary waves-effect waves-light m-l-5">
                                    Cancel
                                </button>
                            </div>

                            {{ Form::close() }}
                        </div>
                    </div><!-- end col -->

                </div>
                <!-- end row -->


            </div> <!-- container -->

        </div> <!-- content -->

        @include('admin-includes.footer')

    </div>







@endsection





@section('scripts')

    <!-- Validation js (Parsleyjs) -->
    <script type="text/javascript" src="{{asset('assets/plugins/parsleyjs/dist/parsley.min.js')}}"></script>

    <script src="{{asset('assets/plugins/select2/js/select2.min.js')}}" type="text/javascript"></script>

    <script type="text/javascript">
        $(".select2").select2();
    </script>

    <script src="{{asset('assets/js/jquery.core.js')}}"></script>
    <script src="{{asset('assets/js/jquery.app.js')}}"></script>

@endsection

@extends('layouts.test-admin-app')

@section('title', 'Add Product')

@section('links')


    <!-- form Uploads -->
    <link href="{{asset('assets/plugins/fileuploads/css/dropify.min.css')}}" rel="stylesheet" type="text/css"/>

    <link href="{{asset('assets/plugins/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css"/>
    <!-- App css -->
    <link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('assets/css/icons.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('assets/css/style.css')}}" rel="stylesheet" type="text/css"/>

    <script src="{{asset('assets/js/modernizr.min.js')}}"></script>

@endsection


@section('content')

    @include('admin-includes.top-bar')

    @include('admin-includes.left-side-bar')


    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container-fluid">

                <div class="row">
                    <div class="col-xl-12">
                        <div class="card-box">
                            <div class="dropdown pull-right">
                                <a href="{{ route('products.index') }}"
                                   class="btn btn-info waves-effect w-md waves-light m-b-5">Product</a>
                            </div>
                            <h4 class="header-title m-t-0 m-b-30" id="heading">Add Product Details</h4>

                            @include ('flash.message')

                            @include ('errors.list')


                            {{ Form::open(array('url' => 'products','data-parsley-validate','novalidate', 'files'=>true, 'id'=>'form' )) }}

                            <div class="form-group row">

                                <div class="col-12 col-md-4">

                                    <label for="brand">Select Brand</label>
                                    <select id="brand" class="form-control select2" name="brand">

                                        <option value="">Please Select Any</option>

                                        @foreach ($brands as $brand)
                                            <option value="{{ $brand->id}}">{{ $brand->name}}</option>
                                        @endforeach

                                    </select>
                                </div>

                                <div class="col-12 col-md-4">

                                    <label for="processor">Select Processor</label>
                                    <select id="processor" class="form-control select2" name="processor">

                                        <option value="">Please Select Any</option>

                                        @foreach ($processors as $processor)
                                            <option value="{{ $processor->id}}">{{ $processor->name}}</option>
                                        @endforeach

                                    </select>
                                </div>

                                <div class="col-12 col-md-4">

                                    <label for="name">Product Name</label>
                                    <input type="text" name="name" parsley-trigger="change"
                                           placeholder="Product Name" class="form-control" id="name">
                                </div>


                            </div>

                            <div class="form-group row">

                                <div class="col-12 col-md-6">

                                    <label for="screen_size">Screen Size (Inches)</label>
                                    <input type="text" name="screen_size" parsley-trigger="change"
                                           placeholder="Screen Size From (Inches)" class="form-control" id="screen_size" >
                                </div>

                                <div class="col-12 col-md-6">

                                    <label for="price">Price ₹</label>
                                    <input type="text"  name="price" parsley-trigger="change"
                                           placeholder="Price" class="form-control" id="price" >
                                </div>


                            </div>

                            <div class="form-group row">
                                <div class="col-12 col-md-4">
                                    <label for="touch_screen">Touch Screen</label>
                                    <select id="touch_screen" class="form-control select2" name="touch_screen">
                                        <option value="1"  >Yes</option>
                                        <option value="0"  >No</option>

                                    </select>
                                </div>

                                <div class="col-12 col-md-4">
                                    <label for="availability">Availability</label>
                                    <select id="availability" class="form-control select2" name="availability">
                                        <option value="1"  >Available</option>
                                        <option value="0"  >Not Available</option>

                                    </select>
                                </div>

                                <div class="col-12 col-md-4">
                                    <label for="status">Status</label>
                                    <select id="status" class="form-control select2" name="status">
                                        <option value="1"  >Active</option>
                                        <option value="0"  >In Active</option>


                                    </select>
                                </div>

                            </div>


                            <div class="form-group row">
                                <div class="col-12 col-md-6">

                                    <label for="image">Product Image</label>
                                    <input type="file" class="dropify form-control" data-default-file="" id="image" name="image" data-max-file-size="2M" />
                                </div>

                            </div>





                            <div class="form-group text-right m-b-0">
                                <button class="btn btn-primary waves-effect waves-light" id="submit" type="submit">
                                    Submit
                                </button>
                                <button type="reset" class="btn btn-secondary waves-effect waves-light m-l-5">
                                    Cancel
                                </button>
                            </div>

                            {{ Form::close() }}
                        </div>
                    </div><!-- end col -->

                </div>
                <!-- end row -->


            </div> <!-- container -->

        </div> <!-- content -->

        @include('admin-includes.footer')

    </div>







@endsection





@section('scripts')


    <!-- Validation js (Parsleyjs) -->
    <script type="text/javascript" src="{{asset('assets/plugins/parsleyjs/dist/parsley.min.js')}}"></script>


    <script src="{{asset('assets/plugins/select2/js/select2.min.js')}}" type="text/javascript"></script>

    <script type="text/javascript">
        $(".select2").select2();
    </script>

    <!-- file uploads js -->
    <script src="{{asset('assets/plugins/fileuploads/js/dropify.min.js')}}"></script>


    <script type="text/javascript">
        $('.dropify').dropify({
            messages: {
                'default': 'Drag and drop a file here or click',
                'replace': 'Drag and drop or click to replace',
                'remove': 'Remove',
                'error': 'Ooops, something wrong appended.'
            },
            error: {
                'fileSize': 'The file size is too big (2M max).'
            }
        });
    </script>



    <script src="{{asset('assets/js/jquery.core.js')}}"></script>
    <script src="{{asset('assets/js/jquery.app.js')}}"></script>

@endsection

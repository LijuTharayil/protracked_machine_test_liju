@extends('layouts.test-admin-app')

 @section('title', 'Product Details')

@section('links')


        <!-- Sweet Alert css -->
        <link href="{{asset('assets/plugins/sweet-alert/sweetalert2.min.css')}}" rel="stylesheet" type="text/css" />


         <!--venobox lightbox-->
         <link rel="stylesheet" href="{{asset('assets/plugins/magnific-popup/dist/magnific-popup.css')}}"/>


        <!-- App css -->
        <link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('assets/css/icons.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('assets/css/style.css')}}" rel="stylesheet" type="text/css" />

        <script src="{{asset('assets/js/modernizr.min.js')}}"></script>

@endsection

@section('content')

     @include('admin-includes.top-bar')

     @include('admin-includes.left-side-bar')


            <div class="content-page">


<div class="content">
                    <div class="container-fluid">
                        <div class="row">
                             @include ('flash.message')
                                   @include ('errors.list')
                        </div>

                        <div class="row">

                            <div class="col-sm-12 col-12">
                                <div class="bg-picture card-box">
                                    <div class="profile-info-name">


                                        @if(!empty($product->image))
                                        <a href="{{ $product->image }}" class="image-popup" title="{{ $product->name }}">

                                         <img src="{{ $product->image }}"
                                             class="img-thumbnail" alt="{{ $product->name }}">
                                        </a>
                                        @endif

                                        <div class="profile-info-detail">
                                            <h4 class="m-0">{{ $product->name }}</h4>
                                            <p class="text-muted m-b-20"><i>{{ $product->brand->name }} || {{ $product->processor->name }}</i></p>


                                            <div class="button-list m-t-20">




                                                <a class="btn btn-warning waves-effect w-md waves-light" href="{{ route('products.edit', $product->id) }}">Edit</a>

                                                    <a href="{{ route('products.index') }}" class="btn btn-info waves-effect w-md waves-light">Products</a>






                                            </div>
                                        </div>

                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                <!--/ meta -->

                                <div class="card-box">


                                    <div class="table-responsive">
                                        <table class="table mb-0">

                                            <tbody>


                                                <tr>
                                                    <td>Brand Name</td>
                                                    <td>{{ $product->brand->name }}</td>
                                                </tr>

                                                <tr>
                                                    <td>Processor Name</td>
                                                    <td>{{ $product->processor->name }}</td>
                                                </tr>


                                                <tr>
                                                    <td>Price</td>
                                                    <td>₹ {{ number_format($product->price,2) }}</td>
                                                </tr>



                                                   <tr>
                                                       <td>Screen Size (Inches)</td>
                                                       <td> {{ number_format($product->screen_size,2) }}</td>
                                                   </tr>

                                                <tr>
                                                    <td>Touch Screen</td>
                                                    <td>  @if($product->touch_screen ==1)
                                                            <span class="badge badge-success">Yes</span>
                                                        @else
                                                            <span class="badge badge-danger">No</span>
                                                        @endif</td>
                                                </tr>


                                                <tr>
                                                    <td>Availability</td>
                                                    <td>  @if($product->availability ==1)
                                                            <span class="badge badge-success">Available</span>
                                                        @else
                                                            <span class="badge badge-danger">Not Available</span>
                                                        @endif</td>
                                                </tr>




                                                <tr>
                                                       <td>Status</td>
                                                       <td>
                                                         @if($product->status ==1)
                                                            <span class="badge badge-success">Active</span>
                                                        @else
                                                            <span class="badge badge-danger">In Active</span>
                                                        @endif

                                                       </td>
                                                   </tr>


                                               @if(!empty($product->created_at))
                                                   <tr>
                                                       <td>Created At</td>
                                                       <td>{{ $product->created_at->format('M d, Y h:i A') }}</td>
                                                   </tr>
                                               @endif
                                               @if(!empty($product->updated_at))
                                                   <tr>
                                                       <td>Updated At</td>
                                                       <td>{{ $product->updated_at->format('M d, Y h:i A') }}</td>
                                                   </tr>
                                               @endif




                                            </tbody>
                                        </table>



                                    </div>
                                </div>

                            </div>



                        </div>

                    </div> <!-- container -->




                </div> <!-- content -->



                 @include('admin-includes.footer')


                 </div>






@endsection





@section('scripts')

        <!-- Magnific popup -->
        <script type="text/javascript" src="{{asset('assets/plugins/magnific-popup/dist/jquery.magnific-popup.min.js')}}"></script>



        <script src="{{asset('assets/plugins/jquery-knob/jquery.knob.js')}}"></script>


        <script type="text/javascript">

            $(document).ready(function() {
                $('.image-popup').magnificPopup({
                    type: 'image',
                    closeOnContentClick: true,
                    mainClass: 'mfp-fade',
                    gallery: {
                        enabled: true,
                        navigateByImgClick: true,
                        preload: [0,1] // Will preload 0 - before current, and 1 after the current image
                    }
                });
            });
        </script>
        <script src="{{asset('assets/js/jquery.core.js')}}"></script>
        <script src="{{asset('assets/js/jquery.app.js')}}"></script>

        @endsection

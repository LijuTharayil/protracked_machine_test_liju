@extends('layouts.test-admin-app')

 @section('title', 'Edit Product')

@section('links')


<!-- form Uploads -->
    <link href="{{asset('assets/plugins/fileuploads/css/dropify.min.css')}}" rel="stylesheet" type="text/css" />
        <!-- App css -->
        <link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('assets/css/icons.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('assets/css/style.css')}}" rel="stylesheet" type="text/css" />

        <script src="{{asset('assets/js/modernizr.min.js')}}"></script>

@endsection


@section('content')

     @include('admin-includes.top-bar')

     @include('admin-includes.left-side-bar')


           <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container-fluid">

                        <div class="row">
                            <div class="col-xl-12">
                                <div class="card-box">
                                      <div class="dropdown pull-right">
                                        <a href="{{ route('products.index') }}" class="btn btn-info waves-effect w-md waves-light m-b-5">Product</a>
                                    </div>
                                    <h4 class="header-title m-t-0 m-b-30">Edit Product Details</h4>

                                     @include ('flash.message')
                                   @include ('errors.list')

                                       {{ Form::model($product, array('route' => array('products.update', $product->id), 'method' => 'PUT', 'data-parsley-validate','novalidate','files'=>true)) }}


                                            <div class="form-group row">


                                                <div class="col-12 col-md-4">

                                                    <label for="brand">Select Brand</label>
                                                    <select id="brand" class="form-control select2" name="brand">

                                                        <option value="">Please Select Any</option>

                                                        @foreach ($brands as $brand)
                                                            <option value="{{ $brand->id}}"  @php if($brand->id == $product->brand_id) { @endphp selected @php } else {} @endphp >{{ $brand->name}}</option>
                                                        @endforeach

                                                    </select>
                                                </div>

                                                <div class="col-12 col-md-4">

                                                    <label for="processor">Select Processor</label>
                                                    <select id="processor" class="form-control select2" name="processor">

                                                        <option value="">Please Select Any</option>

                                                        @foreach ($processors as $processor)
                                                            <option value="{{ $processor->id}}" @php if($processor->id == $product->processor_id) { @endphp selected @php } else {} @endphp>{{ $processor->name}}</option>
                                                        @endforeach

                                                    </select>
                                                </div>

                                                <div class="col-12 col-md-3">

                                                <label for="name">Product Name </label>
                                                <input type="text" name="name" parsley-trigger="change"
                                                placeholder="Product Name" class="form-control" id="name" value="{{ $product->name }}">
                                                </div>



                                        </div>

                                    <div class="form-group row">

                                        <div class="col-12 col-md-6">

                                            <label for="screen_size">Screen Size (Inches)</label>
                                            <input type="number" min="0" name="screen_size" parsley-trigger="change"
                                                   placeholder="Screen Size From (Inches)" class="form-control" id="screen_size" value="{{ $product->screen_size}}" >
                                        </div>

                                        <div class="col-12 col-md-6">

                                            <label for="price">Price ₹</label>
                                            <input type="number" min="0" name="price" parsley-trigger="change"
                                                   placeholder="Price" class="form-control" id="price" value="{{ $product->price}}" >
                                        </div>


                                    </div>

                                    <div class="form-group row">
                                        <div class="col-12 col-md-4">
                                            <label for="touch_screen">Touch Screen</label>
                                            <select id="touch_screen" class="form-control select2" name="touch_screen">
                                                <option value="1"  @php if($product->touch_screen == 1) { @endphp selected @php } else {} @endphp >Yes</option>
                                                <option value="0" @php if($product->touch_screen == 0) { @endphp selected @php } else {} @endphp >No</option>

                                            </select>
                                        </div>

                                        <div class="col-12 col-md-4">
                                            <label for="availability">Availability</label>
                                            <select id="availability" class="form-control select2" name="availability">
                                                <option value="1"  @php if($product->availability == 1) { @endphp selected @php } else {} @endphp >Available</option>
                                                <option value="0" @php if($product->availability == 0) { @endphp selected @php } else {} @endphp >Not Available</option>

                                            </select>
                                        </div>




                                        <div class="col-12 col-md-4">
                                            <label for="status">Status</label>
                                            <select id="status" class="form-control select2" name="status">
                                                <option value="1"  @php if($product->status == 1) { @endphp selected @php } else {} @endphp >Active</option>
                                                <option value="0" @php if($product->status == 0) { @endphp selected @php } else {} @endphp >In Active</option>


                                            </select>
                                        </div>






                                    </div>


                            <div class="form-group row">
                                <div class="col-12 col-md-6">

                                    <label for="image">Product Image</label>
                                    <input type="file" class="dropify form-control" data-default-file="{{ $product->image }}" id="image" name="image" data-max-file-size="2M" />
                                </div>

                            </div>

                                   <div class="form-group text-right m-b-0">
                                            <button class="btn btn-primary waves-effect waves-light" type="submit">
                                                Submit
                                            </button>
                                            <button type="reset" class="btn btn-secondary waves-effect waves-light m-l-5">
                                                Cancel
                                            </button>
                                        </div>

                                    {{ Form::close() }}
                                </div>
                            </div><!-- end col -->

                        </div>
                        <!-- end row -->


                    </div> <!-- container -->

                </div> <!-- content -->

             @include('admin-includes.footer')

            </div>







@endsection





@section('scripts')

                <script type="text/javascript">
 $(".select2").select2();
        </script>

 <!-- file uploads js -->
    <script src="{{asset('assets/plugins/fileuploads/js/dropify.min.js')}}"></script>


    <script type="text/javascript">
        $('.dropify').dropify({
            messages: {
                'default': 'Drag and drop a file here or click',
                'replace': 'Drag and drop or click to replace',
                'remove': 'Remove',
                'error': 'Ooops, something wrong appended.'
            },
            error: {
                'fileSize': 'The file size is too big (2M max).'
            }
        });
    </script>

                <script src="{{asset('assets/js/jquery.core.js')}}"></script>
                <script src="{{asset('assets/js/jquery.app.js')}}"></script>
        @endsection


@extends('layouts.test-admin-app')

 @section('title', 'Products')

@section('links')


        <!-- Sweet Alert css -->
        <link href="{{asset('assets/plugins/sweet-alert/sweetalert2.min.css')}}" rel="stylesheet" type="text/css" />

        <!-- DataTables -->
        <link href="{{asset('assets/plugins/datatables/dataTables.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('assets/plugins/datatables/buttons.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />
        <!-- Responsive datatable examples -->
        <link href="{{asset('assets/plugins/datatables/responsive.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />
        <!-- Multi Item Selection examples -->
        <link href="{{asset('assets/plugins/datatables/select.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />
        <!-- App css -->
        <link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('assets/css/icons.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('assets/css/style.css')}}" rel="stylesheet" type="text/css" />

        <script src="{{asset('assets/js/modernizr.min.js')}}"></script>

@endsection

@section('content')

     @include('admin-includes.top-bar')

     @include('admin-includes.left-side-bar')


            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container-fluid">


                        <div class="row">


                            <div class="col-xl-12">
                                <div class="card-box">



                                  <div class="dropdown pull-right">

                                        <a href="{{ route('products.create') }}" class="btn btn-success waves-effect w-md waves-light m-b-5">Add</a>

                                    </div>

                                    <h4 class="header-title mt-0 m-b-30">Products</h4>

                                     @include ('flash.message')
                                   @include ('errors.list')

                                    <div class="table-responsive">
                                        <table id="responsive-datatable" class="table table-bordered table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                            <thead>
                                            <tr>
                                                <th>Sl No</th>
                                                <th>Product</th>
                                                <th>Price</th>

                                                <th>Screen Size (Inches)</th>
                                                <th>Touch Screen</th>
                                                <th>Availability</th>
                                                <th>Status</th>

                                                <th>Action</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                                @php
                                                $i=1;

                                                @endphp
                                                 @foreach ($products as $product)
                                                <tr>
                                                    <td>{{ $i }}</td>
                                                    <td>{{ $product->name }} || {{ $product->brand->name }} || {{ $product->processor->name }}</td>
                                                    <td>₹ {{ number_format($product->price,2) }}</td>
                                                    <td>{{ number_format($product->screen_size,2) }}</td>
                                                    <td>
                                                        @if($product->touch_screen ==1)
                                                            <span class="badge badge-success">Yes</span>
                                                        @else
                                                            <span class="badge badge-danger">No</span>
                                                        @endif
                                                    </td>

                                                    <td>
                                                        @if($product->availability ==1)
                                                            <span class="badge badge-success">Available</span>
                                                        @else
                                                            <span class="badge badge-danger">Not Available</span>
                                                        @endif
                                                    </td>

                                                    <td>
                                                        @if($product->status ==1)
                                                            <span class="badge badge-success">Active</span>
                                                        @else
                                                            <span class="badge badge-danger">In Active</span>
                                                        @endif
                                                    </td>


                                                    <td>
                                                    {!! Form::open(['method' => 'DELETE', 'route' => ['products.destroy', $product->id],'id' => 'deleteForm'.$product->id, ]) !!}


                                                    <a class="btn btn-warning waves-effect w-md waves-light m-b-5" href="{{ route('products.edit', $product->id) }}">Edit</a>


                                                        @if(0)
                                                            <button type="button" onclick="deleteLock{{$product->id}}()" class="btn btn-danger waves-effect w-md waves-light m-b-5">Lock</button>
                                                        @else
                                                            <button type="button" onclick="deleteConfirm{{$product->id}}()" class="btn btn-danger waves-effect w-md waves-light m-b-5">Delete</button>
                                                        @endif


                                                    <a class="btn btn-info waves-effect w-md waves-light m-b-5" href="{{ route('products.show', $product->id) }}">View</a>

                                                   {!! Form::close() !!}
                                                  </td>


                                                </tr>
                                              @php
                                                $i=$i+1;
                                                @endphp

                                                 @endforeach




                                            </tbody>
                                        </table>



                                    </div>
                                </div>

@if($deleted_products->count())
                                <div class="card-box">




                                <h4 class="header-title mt-0 m-b-30">Deleted Products</h4>


                                <div class="table-responsive">
                                    <table id="responsive-datatable2" class="table table-bordered table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                        <thead>
                                        <tr>
                                            <th>Sl No</th>
                                            <th>Product</th>
                                            <th>Price</th>

                                            <th>Screen Size (Inches)</th>
                                            <th>Touch Screen</th>
                                            <th>Availability</th>
                                            <th>Status</th>

                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @php
                                            $i=1;

                                        @endphp
                                        @foreach ($deleted_products as $deleted_product)
                                            <tr>
                                                <td>{{ $i }}</td>
                                                <td>{{ $deleted_product->name }} || {{ $deleted_product->brand->name }} || {{ $deleted_product->processor->name }}</td>
                                                <td>₹ {{ number_format($deleted_product->price,2) }}</td>
                                                <td>{{ number_format($deleted_product->screen_size,2) }}</td>
                                                <td>
                                                    @if($deleted_product->touch_screen ==1)
                                                        <span class="badge badge-success">Yes</span>
                                                    @else
                                                        <span class="badge badge-danger">No</span>
                                                    @endif
                                                </td>

                                                <td>
                                                    @if($deleted_product->availability ==1)
                                                        <span class="badge badge-success">Available</span>
                                                    @else
                                                        <span class="badge badge-danger">Not Available</span>
                                                    @endif
                                                </td>

                                                <td>
                                                    @if($deleted_product->status ==1)
                                                        <span class="badge badge-success">Active</span>
                                                    @else
                                                        <span class="badge badge-danger">In Active</span>
                                                    @endif
                                                </td>


                                                <td>

                                                    <a class="btn btn-success waves-effect w-md waves-light m-b-5" href="{{ url('restore_product/'. $deleted_product->id) }}">Restore</a>

                                                </td>


                                            </tr>
                                            @php
                                                $i=$i+1;
                                            @endphp

                                        @endforeach




                                        </tbody>
                                    </table>



                                </div>
                            </div>

@endif
                            </div><!-- end col -->

                        </div>
                        <!-- end row -->


                    </div> <!-- container -->

                </div> <!-- content -->

                 @include('admin-includes.footer')


                 </div>






@endsection





@section('scripts')


 <!-- Required datatable js -->

 <script src="{{asset('assets/plugins/datatables/jquery.dataTables.min.js')}}"></script>
 <script src="{{asset('assets/plugins/datatables/dataTables.bootstrap4.min.js')}}"></script>
 <!-- Buttons examples -->
 <script src="{{asset('assets/plugins/datatables/dataTables.buttons.min.js')}}"></script>
 <script src="{{asset('assets/plugins/datatables/buttons.bootstrap4.min.js')}}"></script>
 <script src="{{asset('assets/plugins/datatables/jszip.min.js')}}"></script>
 <script src="{{asset('assets/plugins/datatables/pdfmake.min.js')}}"></script>
 <script src="{{asset('assets/plugins/datatables/vfs_fonts.js')}}"></script>
 <script src="{{asset('assets/plugins/datatables/buttons.html5.min.js')}}"></script>
 <script src="{{asset('assets/plugins/datatables/buttons.print.min.js')}}"></script>

 <!-- Key Tables -->
 <script src="{{asset('assets/plugins/datatables/dataTables.keyTable.min.js')}}"></script>

 <!-- Responsive examples -->
 <script src="{{asset('assets/plugins/datatables/dataTables.responsive.min.js')}}"></script>
 <script src="{{asset('assets/plugins/datatables/responsive.bootstrap4.min.js')}}"></script>

 <!-- Selection table -->
 <script src="{{asset('assets/plugins/datatables/dataTables.select.min.js')}}"></script>


<script src="{{asset('assets/plugins/sweet-alert/sweetalert2.min.js')}}"></script>

 <script type="text/javascript">

@foreach ($products as $product)
      //Warning Message
       function deleteConfirm{{$product->id}}() {
            swal({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#4fa7f3',
                cancelButtonColor: '#d57171',
                confirmButtonText: 'Yes, delete it!'
            }).then(function () {


                swal(
                    'Deleted!',
                    'Your file has been deleted.',
                    'success'

                );

                document.getElementById("deleteForm{{$product->id}}").submit();

            });


        }


//Warning Message
function deleteLock{{$product->id}}() {
    swal({
        title: 'You cant delete now',
        text: "The Driver has many Relations!",
        type: 'warning',
        showCancelButton: false,
        confirmButtonColor: '#4fa7f3',
        confirmButtonText: 'OK, Got It!'
    })


}

        @endforeach
 </script>


        <!-- App js -->
        <script src="{{asset('assets/js/jquery.core.js')}}"></script>
        <script src="{{asset('assets/js/jquery.app.js')}}"></script>


        <script type="text/javascript">

// function deleteConfirm() {
//     var r = confirm("Do You Really Want To Delete");
//     if (r == true) {
//         document.getElementById("deleteForm").submit();

//     } else {

//         location.reload();


//     }

// }

        </script>


 <script type="text/javascript">
     $(document).ready(function () {

         // Default Datatable
         $('#datatable').DataTable();

         //Buttons examples
         var table = $('#datatable-buttons').DataTable({
             lengthChange: false,
             buttons: ['copy', 'excel', 'pdf']
         });

         // Key Tables

         $('#key-table').DataTable({
             keys: true
         });

         // Responsive Datatable
         $('#responsive-datatable').DataTable();
         $('#responsive-datatable2').DataTable();

         // Multi Selection Datatable
         $('#selection-datatable').DataTable({
             select: {
                 style: 'multi'
             }
         });

         table.buttons().container()
             .appendTo('#datatable-buttons_wrapper .col-md-6:eq(0)');
     });

 </script>
 <script src="{{asset('assets/js/jquery.core.js')}}"></script>
 <script src="{{asset('assets/js/jquery.app.js')}}"></script>

        @endsection

@extends('layouts.test-admin-app')

@section('title', 'Home')


@section('links')



    <script src="{{asset('assets/js/modernizr.min.js')}}"></script>


@endsection


@section('content')

    @include('admin-includes.top-bar')


    @include('admin-includes.left-side-bar')

    @include('admin-includes.home-inner-content')

    @include('admin-includes.footer')



@endsection





@section('scripts')

    <!-- App js -->
    <script src="{{asset('assets/pages/jquery.dashboard.js')}}"></script>
    <!-- Counter Up  -->
    <script src="{{asset('assets/plugins/waypoints/jquery.waypoints.min.js')}}"></script>
    <script src="{{asset('assets/plugins/counterup/jquery.counterup.min.js')}}"></script>





    <!-- Dashboard init -->


    <!-- App js -->
    <script src="{{asset('assets/js/jquery.core.js')}}"></script>
    <script src="{{asset('assets/js/jquery.app.js')}}"></script>
@endsection

<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="{{ env('APP_NAME') }}">
    <meta name="author" content="{{ env('APP_NAME') }}">
    <title> {{ env('APP_NAME') }}</title>
    <link rel="shortcut icon" href="{{asset('assets/images/fav.png')}}">

    <!-- App css -->
    <link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/css/icons.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/css/style.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/css/dev.css')}}" rel="stylesheet" type="text/css" />

    <script src="{{asset('assets/js/modernizr.min.js')}}"></script>

</head>


<body class="fixed-left">

<!-- Begin page -->
<div id="wrapper">


    <!-- Top Bar Start -->
    <div class="topbar">

        <!-- LOGO -->
        <div class="topbar-left">
            <a href="" class="logo"><span> {{ env('APP_NAME') }}<span> {{ env('APP_NAME_SUB') }}</span></span><i class="mdi mdi-layers"></i></a>
        </div>

        <!-- Button mobile view to collapse sidebar menu -->
        <div class="navbar navbar-default" role="navigation">
            <div class="container-fluid">

                <!-- Page title -->
                <ul class="nav navbar-nav list-inline navbar-left">
                    <li class="list-inline-item">
                        <button class="button-menu-mobile open-left">
                            <i class="mdi mdi-menu"></i>
                        </button>
                    </li>
                    <li class="list-inline-item">
                        <h4 class="page-title"> Home</h4>
                    </li>
                </ul>

                <nav class="navbar-custom">

                </nav>
            </div><!-- end container -->
        </div><!-- end navbar -->
    </div>
    <!-- Top Bar End -->

    <!-- ========== Left Sidebar Start ========== -->
    <div class="left side-menu">
        <div class="sidebar-inner slimscrollleft">


            <!-- End User -->

            <!--- Sidemenu -->
            <div id="sidebar-menu">
                {{ Form::open(array('url' => '','data-parsley-validate','novalidate', 'files'=>true)) }}
                <div class="container">
                    <div class="form-group row">
                        <div class="col-12 col-md-12 mt-5">
                            <p>Price Range</p>
                            <section class="range-slider">
                                <span class="rangeValues"></span>
                                <input value="{{ $price->from }}" name="price_from" onchange="ajaxFilter()" min="{{ $price->from }}" max="{{ $price->to }}" step="{{ $price->step }}" type="range">
                                <input value="{{ $price->to }}" name="price_to" onchange="ajaxFilter()" min="{{ $price->from }}" max="{{ $price->to }}" step="{{ $price->step }}" type="range">
                            </section>
                        </div>
                        </div>

                    <div class="form-group row">
                        <div class="col-12 col-md-12 mt-5">

                            <label for="brand">Touch Screen</label>
                            @foreach ($brands as $brand )
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" onclick="ajaxFilter()" name="brand[]" class="custom-control-input brand_id_class" id="brand{{ $brand->id }}" value="{{ $brand->id }}">
                                    <label class="custom-control-label" for="brand{{ $brand->id }}">{{ $brand->name }}</label>
                                </div><br>
                            @endforeach
                        </div>
                    </div>


                    <div class="form-group row">
                        <div class="col-12 col-md-12 mt-5">

                            <label for="brand">Processors</label>
                            @foreach ($processors as $processor )
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" onclick="ajaxFilter()" name="processor[]" class="custom-control-input processor_id_class" id="processor{{ $processor->id }}" value="{{ $processor->id }}">
                                    <label class="custom-control-label" for="processor{{ $processor->id }}">{{ $processor->name }}</label>
                                </div><br>
                            @endforeach
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-12 col-md-12 mt-5">

                            <label for="brand">Screen Size</label>
                            @foreach ($screens as $screen )
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" onclick="ajaxFilter()" name="screen[]" class="custom-control-input screen_id_class" id="screen{{ $screen->id }}" value="{{ $screen->id }}">
                                    <label class="custom-control-label" for="screen{{ $screen->id }}">{{ $screen->name }}</label>
                                </div><br>
                            @endforeach
                        </div>
                    </div>



                    <div class="form-group row">
                        <div class="col-12 col-md-12 mt-5">

                            <label for="brand">Touch Screen</label>
                            <select id="touch_screen" onchange="ajaxFilter()" class="form-control select2" name="touch_screen">

                                <option value="2">Select Any</option>
                                <option value="1">Yes</option>
                                <option value="0">No</option>

                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-12 col-md-12 mt-5">

                            <label for="brand">Availability</label>
                            <select id="availability" onchange="ajaxFilter()" class="form-control select2" name="availability">

                                <option value="2">Select Any</option>
                                <option value="1">Available</option>
                                <option value="0">Not Available</option>

                            </select>
                        </div>
                    </div>

                    </div>


                </div>
            {{ Form::close() }}

            </div>

                <div class="clearfix"></div>
            </div>
            <!-- Sidebar -->
            <div class="clearfix"></div>

        </div>

    </div>
    <!-- Left Sidebar End -->



    <!-- ============================================================== -->
    <!-- Start right Content here -->
    <!-- ============================================================== -->
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container-fluid" id="product_container">

                @if($product_items->count())
                <div class="port m-b-20">
                    <div class="portfolioContainer row">

                        @foreach ($product_items as $product)
                        <div class="col-md-6 col-xl-3 col-lg-4 natural personal">
                            <div class="gal-detail thumb">
                                <a href="" class="image-popup" title="{{ $product->name }}">
                                    <img src="{{ $product->image }}" class="thumb-img" alt="work-thumbnail">
                                </a>
                                <h4>{{ $product->name }}</h4>
                                <p class="text-muted text-dark">
                                    Brand :<strong> {{ $product->brand->name }}</strong> <br>
                                    Processor :<strong> {{ $product->processor->name }} </strong><br>
                                    Price :<strong> ₹ {{ number_format($product->price,2) }} </strong><br>
                                    Screen Size :<strong> {{ number_format($product->screen_size,2) }} </strong><br>
                                    Availability :  @if($product->availability ==1)
                                        <span class="badge badge-success">Available</span>
                                    @else
                                        <span class="badge badge-danger">Not Available</span>
                                    @endif <br>
                                    Touch Screen :     @if($product->touch_screen ==1)
                                        <span class="badge badge-success">Yes</span>
                                    @else
                                        <span class="badge badge-danger">No</span>
                                    @endif <br>
                                </p>
                            </div>
                        </div>
                      @endforeach
                            {{ $product_items->links() }}

                    </div>
                </div>

                @else
                    <div class="port m-b-20">
                        <div class="portfolioContainer">


                                    <div class="col-md-12 col-xl-12 col-lg-12">
                                        <div class="gal-detail thumb">
                                            <a href="" class="image-popup" title="404">
                                                <img src="{{asset('assets/images/404.png')}}" class="thumb-img" alt="404">
                                            </a>

                                        </div>
                                    </div>



                        </div>
                    </div>

             @endif

            </div> <!-- container -->

        </div> <!-- content -->

        <footer class="footer text-right">
            &copy; {{ date('Y')  }} {{ env('APP_NAME') }} All Rights Reserved
        </footer>

    </div>


    <!-- ============================================================== -->
    <!-- End Right content here -->
    <!-- ============================================================== -->




</div>
<!-- END wrapper -->

<!-- jQuery  -->
<script src="{{asset('assets/js/jquery.min.js')}}"></script>
<script src="{{asset('assets/js/popper.min.js')}}"></script>
<script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
<script src="{{asset('assets/js/detect.js')}}"></script>
<script src="{{asset('assets/js/fastclick.js')}}"></script>
<script src="{{asset('assets/js/jquery.blockUI.js')}}"></script>
<script src="{{asset('assets/js/waves.js')}}"></script>
<script src="{{asset('assets/js/jquery.nicescroll.js')}}"></script>
<script src="{{asset('assets/js/jquery.slimscroll.js')}}"></script>
<script src="{{asset('assets/js/jquery.scrollTo.min.js')}}"></script>

<!-- isotope filter plugin -->
<script type="text/javascript" src="{{asset('assets/plugins/isotope/dist/isotope.pkgd.min.js')}}"></script>

<!-- Magnific popup -->
<script type="text/javascript" src="{{asset('assets/plugins/magnific-popup/dist/jquery.magnific-popup.min.js')}}"></script>

<!-- App js -->
<script src="{{asset('assets/js/jquery.core.js')}}"></script>
<script src="{{asset('assets/js/jquery.app.js')}}"></script>


<script src="{{asset('assets/js/dev.js')}}"></script>

<script>

    function getVals(){
        // Get slider values
        var parent = this.parentNode;
        var slides = parent.getElementsByTagName("input");
        var slide1 = parseFloat( slides[0].value );
        var slide2 = parseFloat( slides[1].value );
        // Neither slider will clip the other, so make sure we determine which is larger
        if( slide1 > slide2 ){ var tmp = slide2; slide2 = slide1; slide1 = tmp; }

        var displayElement = parent.getElementsByClassName("rangeValues")[0];
        displayElement.innerHTML = "₹  " + slide1 + "  -  ₹  " + slide2;


    }

    window.onload = function(){
        // Initialize Sliders
        var sliderSections = document.getElementsByClassName("range-slider");
        for( var x = 0; x < sliderSections.length; x++ ){
            var sliders = sliderSections[x].getElementsByTagName("input");
            for( var y = 0; y < sliders.length; y++ ){
                if( sliders[y].type ==="range" ){
                    sliders[y].oninput = getVals;
                    // Manually trigger event first time to display values
                    sliders[y].oninput();
                }
            }
        }
    }

    function  ajaxFilter() {

        var brand = [];
        $('.brand_id_class:checked').each(function(){
            var b = parseInt($(this).val());
            brand .push(b);
        })

        var processor = [];
        $('.processor_id_class:checked').each(function(){
            var p = parseInt($(this).val());
            processor .push(p);
        })

        var screens = [];
        $('.screen_id_class:checked').each(function(){
            var s = parseInt($(this).val());
            screens .push(s);
        })

        var token = $("input[name='_token']").val();
        var price_from = $("input[name='price_from']").val();
        var price_to = $("input[name='price_to']").val();
        var touch_screen = $("select[name='touch_screen']").val();
        var availability = $("select[name='availability']").val();


        $.ajax({
            url: "<?php echo route('select_ajax_items') ?>",
            method: 'POST',
            data: { price_from:price_from,
                price_to:price_to,
                brand:brand,
                processor:processor,
                screens:screens,
                touch_screen:touch_screen,
                availability:availability,
                _token:token},
            success: function(data) {
                $("#product_container").html(data.options);
            }
        });

    }
</script>


</body>
</html>

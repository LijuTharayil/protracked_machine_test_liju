      <div class="left side-menu">
                <div class="sidebar-inner slimscrollleft">

                    <!-- User -->
                    <div class="user-box">
                        <div class="user-img">
                            <img src="{{asset('assets/images/users/avatar-1.jpg')}}" alt="user-img" title="{{ Auth::user()->name }}" class="rounded-circle img-thumbnail img-responsive">
                            <div class="user-status offline"><i class="mdi mdi-adjust"></i></div>
                        </div>

                        <h5><a href="#">{{ Auth::user()->name }}</a> </h5>
                        <ul class="list-inline">
                            <li class="list-inline-item">
                                <a href="" >
                                    <i class="mdi mdi-settings"></i>
                                </a>
                            </li>

                            <li class="list-inline-item">
                               <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();"class="text-custom">
                                    <i class="mdi mdi-power"></i>
                                </a>
                                 <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                            </li>
                        </ul>
                    </div>
                    <!-- End User -->

                    <!--- Sidemenu -->
                    <div id="sidebar-menu">
                        <ul>

                            <li>
                                <a href="{{ route('home') }}" class="waves-effect"><i class="mdi mdi-home"></i> <span> Home </span> </a>
                            </li>

                            <li>
                                <a href="{{ route('brands.index') }}" class="waves-effect"><i class="mdi mdi-star-circle"></i> <span> Brands </span> </a>
                            </li>
                            <li>
                                <a href="{{ route('processors.index') }}" class="waves-effect"><i class="mdi mdi-star-circle"></i> <span> Processors </span> </a>
                            </li>
                            <li>
                                <a href="{{ route('products.index') }}" class="waves-effect"><i class="mdi mdi-star-circle"></i> <span> Products </span> </a>
                            </li>
                            <li>
                                <a href="{{ route('screens.index') }}" class="waves-effect"><i class="mdi mdi-star-circle"></i> <span> Screen Size </span> </a>
                            </li>
                            <li>
                                <a href="{{ route('prices.index') }}" class="waves-effect"><i class="mdi mdi-star-circle"></i> <span> Price Range </span> </a>
                            </li>




                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <!-- Sidebar -->
                    <div class="clearfix"></div>

                </div>

            </div>

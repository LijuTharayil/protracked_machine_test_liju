
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container-fluid">

                       <div class="row">
                            <div class="col-xl-4 col-sm-6 col-6">
                                <div class="card-box widget-user">
                                    <div class="text-center">
                                        <h2 class="text-custom" data-plugin="counterup">{{ $brands }}</h2>
                                        <h5>Brands</h5>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xl-4 col-sm-6 col-6">
                                <div class="card-box widget-user">
                                    <div class="text-center">
                                        <h2 class="text-pink" data-plugin="counterup">{{ $processors }}</h2>
                                        <h5>Processors</h5>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xl-4 col-sm-6 col-6">
                                <div class="card-box widget-user">
                                    <div class="text-center">
                                        <h2 class="text-info" data-plugin="counterup">{{ $products }}</h2>
                                        <h5>Products</h5>
                                    </div>
                                </div>
                            </div>


                        </div>


                    </div> <!-- container -->

                </div> <!-- content -->

                 </div>

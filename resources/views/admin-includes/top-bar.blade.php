 <div class="topbar">

                <!-- LOGO -->
                <div class="topbar-left">
                    <a href="{{ route('home') }}" class="logo"><span>{{ env('APP_NAME') }}<span>{{ env('APP_NAME_SUB') }}</span></span><i class="mdi mdi-layers"></i></a>
                </div>

                <!-- Button mobile view to collapse sidebar menu -->
                <div class="navbar navbar-default" role="navigation">
                    <div class="container-fluid">


 <!-- Page title -->
                        <ul class="nav navbar-nav list-inline navbar-left">
                            <li class="list-inline-item">
                                <button class="button-menu-mobile open-left">
                                    <i class="mdi mdi-menu"></i>
                                </button>
                            </li>
                            <li class="list-inline-item">
                                <h4 class="page-title"> @yield('title')</h4>
                            </li>
                        </ul>




                    </div><!-- end container -->
                </div><!-- end navbar -->
            </div>

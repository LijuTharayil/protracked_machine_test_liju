@extends('layouts.test-login-app')

@section('content')
    <div class="ex-page-content text-center">
        <div class="text-error">Oops !</div>
        <h3 class="text-uppercase font-600">You Are Not Verified</h3>
        <p class="text-muted">
            It's looking like you may have taken a wrong turn. Don't worry... it happens to
            the best of us. You might want to check your internet connection. Here's a little tip that might
            help you get back on track.
        </p>


        <a href="{{ route('logout') }}" class="btn btn-success waves-effect w-md waves-light m-b-5"
                     onclick="event.preventDefault();
                              document.getElementById('logout-form').submit();"class="text-custom">
             <i class="mdi mdi-power"></i>
         </a>



          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                     {{ csrf_field() }}
                 </form>

    </div>

@endsection

@extends('layouts.test-login-app')

@section('content')
	<div class="ex-page-content text-center">
                <div class="text-error">401</div>
                <h3 class="text-uppercase font-600">Access Denied</h3>
                <p class="text-muted">
                    It's looking like you may have taken a wrong turn. Don't worry... it happens to
                    the best of us. You might want to check your internet connection. Here's a little tip that might
                    help you get back on track.
                </p>
                <br>
                <a class="btn btn-success waves-effect waves-light" href="{{ route('home') }}"> Return Home</a>

            </div>

@endsection

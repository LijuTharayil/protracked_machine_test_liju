@if (count($errors) > 0)
@foreach ($errors as $error)
 <div class="col-12">
 <div class="alert alert-danger alert-dismissable">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    {{ $error }}
</div>
</div>
@endforeach

 @endif

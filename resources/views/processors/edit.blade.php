@extends('layouts.test-admin-app')

@section('title', 'Edit Processors')

@section('links')

    <link href="{{asset('assets/plugins/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css" />


@endsection


@section('content')

    @include('admin-includes.top-bar')

    @include('admin-includes.left-side-bar')


    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container-fluid">

                <div class="row">
                    <div class="col-xl-12">
                        <div class="card-box">
                            <div class="dropdown pull-right">
                                <a href="{{ route('processors.index') }}" class="btn btn-info waves-effect w-md waves-light m-b-5">Processors</a>
                            </div>
                            <h4 class="header-title m-t-0 m-b-30">Edit Processors Details</h4>

                            @include ('flash.message')
                            @include ('errors.list')

                            {{ Form::model($processor, array('route' => array('processors.update', $processor->id), 'method' => 'PUT', 'data-parsley-validate','novalidate')) }}



                            <div class="form-group row">

                                <div class="col-12 col-md-6">

                                    <label for="name">Processor Name</label>
                                    <input type="text" name="name" parsley-trigger="change"
                                           placeholder="Enter Processor Name" class="form-control" id="name" value="{{ $processor->name }}">
                                </div>

                                <div class="col-12 col-md-6">
                                    <label for="branch_id">Status</label>
                                    <select id="status" class="form-control select2" name="status">
                                        <option value="1"  @php if($processor->status == 1) { @endphp selected @php } else {} @endphp >Active</option>
                                        <option value="0" @php if($processor->status == 0) { @endphp selected @php } else {} @endphp >In Active</option>


                                    </select>

                                </div>






                            </div>

                            <div class="form-group text-right m-b-0">
                                <button class="btn btn-primary waves-effect waves-light" type="submit">
                                    Submit
                                </button>
                                <button type="reset" class="btn btn-secondary waves-effect waves-light m-l-5">
                                    Cancel
                                </button>
                            </div>

                            {{ Form::close() }}
                        </div>
                    </div><!-- end col -->

                </div>
                <!-- end row -->


            </div> <!-- container -->

        </div> <!-- content -->

        @include('admin-includes.footer')

    </div>







@endsection





@section('scripts')

    <!-- Validation js (Parsleyjs) -->
    <script type="text/javascript" src="{{asset('assets/plugins/parsleyjs/dist/parsley.min.js')}}"></script>

    <script src="{{asset('assets/plugins/select2/js/select2.min.js')}}" type="text/javascript"></script>

    <script type="text/javascript">
        $(".select2").select2();
    </script>

    <script src="{{asset('assets/js/jquery.core.js')}}"></script>
    <script src="{{asset('assets/js/jquery.app.js')}}"></script>

@endsection


@extends('layouts.test-admin-app')

 @section('title', 'Brands')

@section('links')


        <!-- Sweet Alert css -->
        <link href="{{asset('assets/plugins/sweet-alert/sweetalert2.min.css')}}" rel="stylesheet" type="text/css" />

        <!-- DataTables -->
        <link href="{{asset('assets/plugins/datatables/dataTables.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('assets/plugins/datatables/buttons.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />
        <!-- Responsive datatable examples -->
        <link href="{{asset('assets/plugins/datatables/responsive.bootstrap4.min.css')}}" rel="stylesheet" type="text/css" />


@endsection

@section('content')

     @include('admin-includes.top-bar')

     @include('admin-includes.left-side-bar')


            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container-fluid">


                        <div class="row">


                            <div class="col-xl-12">
                                <div class="card-box">



                                  <div class="dropdown pull-right">

                                        <a href="{{ route('brands.create') }}" class="btn btn-success waves-effect w-md waves-light m-b-5">Add</a>

                                    </div>

                                    <h4 class="header-title mt-0 m-b-30">Brands</h4>

                                     @include ('flash.message')
                                   @include ('errors.list')

                                    <div class="table-responsive">
                                        <table id="responsive-datatable" class="table table-bordered table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                                            <thead>
                                            <tr>
                                                <th>Sl No</th>
                                                <th>Brand Name</th>

                                                <th>Status</th>

                                                <th>Updated At</th>

                                                <th>Action</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                                @php
                                                $i=1;

                                                @endphp
                                                 @foreach ($brands as $brand)
                                                <tr>
                                                    <td>{{ $i }}</td>
                                                    <td>{{ $brand->name }}</td>

                                                    <td>
                                                        @if($brand->status ==1)
                                                            <span class="badge badge-success">Active</span>
                                                        @else
                                                            <span class="badge badge-danger">In Active</span>
                                                        @endif
                                                    </td>


                                                    <td>{{ $brand->updated_at->format('M d, Y h:i A') }}</td>


                                                    <td>
                                                    {!! Form::open(['method' => 'DELETE', 'route' => ['brands.destroy', $brand->id],'id' => 'deleteForm'.$brand->id, ]) !!}


                                                    <a class="btn btn-warning waves-effect w-md waves-light m-b-5" href="{{ route('brands.edit', $brand->id) }}">Edit</a>



                                                        @if ($brand->products()->count())
                                                            <button type="button" onclick="deleteLock{{$brand->id}}()" class="btn btn-danger waves-effect w-md waves-light m-b-5">Lock</button>
                                                        @else
                                                            <button type="button" onclick="deleteConfirm{{$brand->id}}()" class="btn btn-danger waves-effect w-md waves-light m-b-5">Delete</button>
                                                        @endif




                                                   {!! Form::close() !!}
                                                  </td>


                                                </tr>
                                              @php
                                                $i=$i+1;
                                                @endphp

                                                 @endforeach




                                            </tbody>
                                        </table>



                                    </div>
                                </div>
                            </div><!-- end col -->

                        </div>
                        <!-- end row -->


                    </div> <!-- container -->

                </div> <!-- content -->

                 @include('admin-includes.footer')


                 </div>






@endsection





@section('scripts')

 <!-- Required datatable js -->

 <script src="{{asset('assets/plugins/datatables/jquery.dataTables.min.js')}}"></script>
 <script src="{{asset('assets/plugins/datatables/dataTables.bootstrap4.min.js')}}"></script>
 <!-- Buttons examples -->
 <script src="{{asset('assets/plugins/datatables/dataTables.buttons.min.js')}}"></script>
 <script src="{{asset('assets/plugins/datatables/buttons.bootstrap4.min.js')}}"></script>
 <script src="{{asset('assets/plugins/datatables/jszip.min.js')}}"></script>
 <script src="{{asset('assets/plugins/datatables/pdfmake.min.js')}}"></script>
 <script src="{{asset('assets/plugins/datatables/vfs_fonts.js')}}"></script>
 <script src="{{asset('assets/plugins/datatables/buttons.html5.min.js')}}"></script>
 <script src="{{asset('assets/plugins/datatables/buttons.print.min.js')}}"></script>

 <!-- Key Tables -->
 <script src="{{asset('assets/plugins/datatables/dataTables.keyTable.min.js')}}"></script>

 <!-- Responsive examples -->
 <script src="{{asset('assets/plugins/datatables/dataTables.responsive.min.js')}}"></script>
 <script src="{{asset('assets/plugins/datatables/responsive.bootstrap4.min.js')}}"></script>

 <!-- Selection table -->
 <script src="{{asset('assets/plugins/datatables/dataTables.select.min.js')}}"></script>


<script src="{{asset('assets/plugins/sweet-alert/sweetalert2.min.js')}}"></script>

 <script type="text/javascript">

@foreach ($brands as $brand)
      //Warning Message
       function deleteConfirm{{$brand->id}}() {
            swal({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#4fa7f3',
                cancelButtonColor: '#d57171',
                confirmButtonText: 'Yes, delete it!'
            }).then(function () {


                swal(
                    'Deleted!',
                    'Your file has been deleted.',
                    'success'

                );

                document.getElementById("deleteForm{{$brand->id}}").submit();

            });


        }


//Warning Message
function deleteLock{{$brand->id}}() {
    swal({
        title: 'You cant delete now',
        text: "The Brand has many Relations!",
        type: 'warning',
        showCancelButton: false,
        confirmButtonColor: '#4fa7f3',
        confirmButtonText: 'OK, Got It!'
    })


}

        @endforeach
 </script>


        <script type="text/javascript">

// function deleteConfirm() {
//     var r = confirm("Do You Really Want To Delete");
//     if (r == true) {
//         document.getElementById("deleteForm").submit();

//     } else {

//         location.reload();


//     }

// }

        </script>


 <script type="text/javascript">
     $(document).ready(function () {

         // Default Datatable
         $('#datatable').DataTable();

         //Buttons examples
         var table = $('#datatable-buttons').DataTable({
             lengthChange: false,
             buttons: ['copy', 'excel', 'pdf']
         });

         // Key Tables

         $('#key-table').DataTable({
             keys: true
         });

         // Responsive Datatable
         $('#responsive-datatable').DataTable();

         // Multi Selection Datatable
         $('#selection-datatable').DataTable({
             select: {
                 style: 'multi'
             }
         });

         table.buttons().container()
             .appendTo('#datatable-buttons_wrapper .col-md-6:eq(0)');
     });

 </script>
 <script src="{{asset('assets/js/jquery.core.js')}}"></script>
 <script src="{{asset('assets/js/jquery.app.js')}}"></script>

        @endsection

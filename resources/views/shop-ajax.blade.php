<div class="container-fluid" id="product_container">

                @if($product_items->count())
                <div class="port m-b-20">
                    <div class="portfolioContainer row">

                        @foreach ($product_items as $product)
                        <div class="col-md-6 col-xl-3 col-lg-4 natural personal">
                            <div class="gal-detail thumb">
                                <a href="" class="image-popup" title="{{ $product->name }}">
                                    <img src="{{ $product->image }}" class="thumb-img" alt="work-thumbnail">
                                </a>
                                <h4>{{ $product->name }}</h4>
                                <p class="text-muted text-dark">
                                    Brand :<strong> {{ $product->brand->name }}</strong> <br>
                                    Processor :<strong> {{ $product->processor->name }} </strong><br>
                                    Price :<strong> ₹ {{ number_format($product->price,2) }} </strong><br>
                                    Screen Size :<strong> {{ number_format($product->screen_size,2) }} </strong><br>
                                    Availability :  @if($product->availability ==1)
                                        <span class="badge badge-success">Available</span>
                                    @else
                                        <span class="badge badge-danger">Not Available</span>
                                    @endif <br>
                                    Touch Screen :     @if($product->touch_screen ==1)
                                        <span class="badge badge-success">Yes</span>
                                    @else
                                        <span class="badge badge-danger">No</span>
                                    @endif <br>
                                </p>
                            </div>
                        </div>
                      @endforeach



                    </div>
                </div>

                @else
                    <div class="port m-b-20">
                        <div class="portfolioContainer">


                                    <div class="col-md-12 col-xl-12 col-lg-12">
                                        <div class="gal-detail thumb">
                                            <a href="" class="image-popup" title="404">
                                                <img src="{{asset('assets/images/404.png')}}" class="thumb-img" alt="404">
                                            </a>

                                        </div>
                                    </div>



                        </div>
                    </div>

             @endif

            </div> <!-- container -->

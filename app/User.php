<?php

namespace App;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'contact', 'role', 'verified', 'status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];



    public function screens()
    {
        return $this->hasMany('App\Screen');
    }

    public function products()
    {
        return $this->hasMany('App\Product');
    }


    public function brands()
    {
        return $this->hasMany('App\Brand');
    }

    public function processors()
    {
        return $this->hasMany('App\Processor');
    }

    public function prices()
    {
        return $this->hasMany('App\Price');
    }


    public function getUserName()
    {
        return $this->name;
    }

    public function getUserId()
    {
        return $this->id;
    }

}

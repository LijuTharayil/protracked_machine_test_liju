<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class Product extends Model implements HasMedia
{
    //
    use SoftDeletes;
    use HasMediaTrait;
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'name','price', 'screen_size', 'availability', 'touch_screen', 'brand_id', 'processor_id', 'user_id', 'status'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function processor()
    {
        return $this->belongsTo('App\Processor');
    }
    public function brand()
    {
        return $this->belongsTo('App\Brand');
    }

    public function getImageAttribute()
    {
        return $this->getFirstMediaUrl('product_image');
    }

}

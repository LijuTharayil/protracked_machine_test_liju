<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Screen extends Model
{
    //
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable = [ 'name','from', 'to', 'status', 'user_id'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}

<?php

namespace App\Http\Controllers;

use App\Processor;
use Illuminate\Http\Request;

class ProcessorController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        //
        $processors = Processor::orderby('name', 'asc')->get();

        return view('processors.index', compact('processors'));

    }


    public function create()
    {
        //
        return view('processors.create');

    }


    public function store(Request $request)
    {
        //
        $this->validate($request, [
            'name'=>'required|max:120',
            'status'=>'required',
        ]);

        $processor = new Processor();

        $processor->name = $request->input('name');

        $processor->status = $request->input('status');

        $processor->user_id = auth()->user()->id;

        $processor->save();


        return redirect()->route('processors.index')
            ->with('flash_message',
                'Processor Created');


    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        //
        $processor = Processor::findOrFail($id);

        return view('processors.edit', compact('processor'));
    }


    public function update(Request $request, $id)
    {
        //
        $processor = Processor::findOrFail($id);
        $this->validate($request, [
            'name'=>'required|max:120',
            'status'=>'required',
        ]);


        $processor->name = $request->input('name');

        $processor->status = $request->input('status');

        $processor->user_id = auth()->user()->id;

        $processor->save();


        return redirect()->route('processors.index',
            $id)->with('flash_message',
            'Processor Updated');


    }


    public function destroy($id)
    {
        //
        $processor = Processor::findOrFail($id);
        $processor->delete();

        return redirect()->route('processors.index')
            ->with('flash_message',
                'Processor Deleted.');
    }
}

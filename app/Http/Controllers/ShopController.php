<?php

namespace App\Http\Controllers;

use App\Brand;
use App\Price;
use App\Processor;
use App\Product;
use App\Screen;
use Illuminate\Http\Request;

class ShopController extends Controller
{
    //

    public function getProducts(Request $request)
    {

        $product_items = Product::orderby('id', 'desc')->where('status', '=', 1)->paginate(12);
        $brands = Brand::orderby('name', 'asc')->where('status', '=', 1)->get();
        $processors = Processor::orderby('name', 'asc')->where('status', '=', 1)->get();
        $screens = Screen::orderby('name', 'asc')->where('status', '=', 1)->get();
        $price = Price::orderby('id', 'desc')->where('status', '=', 1)->first();


        return view('shop', compact('product_items','brands', 'processors', 'screens', 'price'));
    }

    public function ajaxFilter (Request $request)
    {


        if($request->ajax()){


            $product_items =Product::orderby('id', 'desc')->where('status', '=', 1);
            if(isset($request->brand))
            {
                $product_items->whereIn('brand_id',$request->brand);
            }
            if(isset($request->processor))
            {
                $product_items->whereIn('processor_id',$request->processor);
            }
            if(isset($request->screens))
            {
                $selected_screens = Screen::whereIn('id',$request->screens)->get();

                foreach ($selected_screens as $selected_screen)
                {

                     $product_items->whereBetween('screen_size', [$selected_screen->from, $selected_screen->to]);
                }

            }
            $product_items->whereBetween('price', [$request->price_from, $request->price_to]);
            if($request->touch_screen != 2)
            {
                $product_items->where('touch_screen',$request->touch_screen);
            }

            if($request->availability != 2)
            {
                $product_items->where('availability',$request->availability);
            }

            $product_items = $product_items->paginate(12);



            $data = view('shop-ajax',compact('product_items'))->render();

            return response()->json(['options'=>$data, 'options2'=>$data]);
        }
    }
}

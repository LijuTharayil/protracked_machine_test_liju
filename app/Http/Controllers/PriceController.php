<?php

namespace App\Http\Controllers;

use App\Price;
use Illuminate\Http\Request;

class PriceController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        //
        $prices = Price::orderby('id', 'desc')->get();
        return view('prices.index', compact('prices'));

    }


    public function create()
    {
        //
        if(Price::count() == 1) return redirect()->route('prices.index');
        return view('prices.create');

    }


    public function store(Request $request)
    {
        //
        $this->validate($request, [

            'from' => 'required|numeric',
            'to' => 'required|numeric|gt:from',
            'step' => 'required|numeric|gte:from|lt:to',
            'status'=>'required',
        ]);

        $price = new Price();


        $price->status = $request->input('status');
        $price->step = $request->input('step');
        $price->from = $request->input('from');
        $price->to = $request->input('to');

        $price->user_id = auth()->user()->id;

        $price->save();


        return redirect()->route('prices.index')
            ->with('flash_message',
                'Price Created');


    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        //
        $price = Price::findOrFail($id);

        return view('prices.edit', compact('price'));
    }


    public function update(Request $request, $id)
    {
        //
        $price = Price::findOrFail($id);
        $this->validate($request, [

            'from' => 'required|numeric',
            'to' => 'required|numeric|gt:from',
            'step' => 'required|numeric|gte:from|lt:to',
            'status'=>'required',
        ]);

        $price->status = $request->input('status');
        $price->from = $request->input('from');
        $price->step = $request->input('step');
        $price->to = $request->input('to');
        $price->user_id = auth()->user()->id;

        $price->save();


        return redirect()->route('prices.index',
            $id)->with('flash_message',
            'Price Updated');


    }


    public function destroy($id)
    {
        //
        $price = Price::findOrFail($id);
        $price->delete();

        return redirect()->route('prices.index')
            ->with('flash_message',
                'Price Deleted.');
    }
}

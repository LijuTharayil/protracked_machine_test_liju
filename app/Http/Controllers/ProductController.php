<?php

namespace App\Http\Controllers;

use App\Brand;
use App\Processor;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Validator;

class ProductController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        //
        $products = Product::orderby('name', 'asc')->get();

        $deleted_products = Product::orderby('name', 'asc')->onlyTrashed()->get();

        return view('products.index', compact('products', 'deleted_products'));

    }


    public function create()
    {
        //
        $brands = Brand::orderby('name', 'asc')->get();
        $processors = Processor::orderby('name', 'asc')->get();
        return view('products.create', compact('brands', 'processors'));

    }


    public function store(Request $request)
    {
        //
        $this->validate($request, [
            'name'=>'required|max:120',
            'price' => 'required|numeric|min:0|between:0.00,99999999.99',
            'screen_size' => 'required|numeric|between:0.00,999.99',
            'status'=>'required',
            'availability'=>'required',
            'touch_screen'=>'required',
            'brand'=>'required',
            'processor'=>'required',
            'image' => 'image|required|mimes:jpeg,png,jpg,gif,svg',
        ]);

        $product = new Product();


        $product->name = $request->input('name');
        $product->status = $request->input('status');
        $product->price = $request->input('price');
        $product->screen_size = $request->input('screen_size');
        $product->availability = $request->input('availability');
        $product->touch_screen = $request->input('touch_screen');
        $product->brand_id = $request->input('brand');
        $product->processor_id = $request->input('processor');
        $product->user_id = auth()->user()->id;
        if (Input::file('image')) {
            $product->addMedia($request->file('image'))->toMediaCollection('product_image');
        }

        $product->save();


        return redirect()->route('products.index')
            ->with('flash_message',
                'Product Created');


    }


    public function show($id)
    {
        //
        $product = Product::findOrFail($id);

        return view('products.show', compact('product'));
    }




    public function edit($id)
    {
        //
        $product = Product::findOrFail($id);
        $brands = Brand::orderby('name', 'asc')->get();
        $processors = Processor::orderby('name', 'asc')->get();
        return view('products.edit', compact('product','brands', 'processors'));
    }


    public function update(Request $request, $id)
    {
        //
        $product = Product::findOrFail($id);
        $this->validate($request, [
            'name'=>'required|max:120',
            'price' => 'required|numeric|min:0|between:0.00,99999999.99',
            'screen_size' => 'required|numeric|between:0.00,999.99',
            'status'=>'required',
            'availability'=>'required',
            'touch_screen'=>'required',
            'brand'=>'required',
            'processor'=>'required',
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg',
        ]);


        $product->name = $request->input('name');
        $product->status = $request->input('status');
        $product->price = $request->input('price');
        $product->screen_size = $request->input('screen_size');
        $product->availability = $request->input('availability');
        $product->touch_screen = $request->input('touch_screen');
        $product->brand_id = $request->input('brand');
        $product->processor_id = $request->input('processor');

        $product->user_id = auth()->user()->id;


        if (Input::file('image')) {
            $product->addMedia($request->file('image'))->toMediaCollection('product_image');
        }

        $product->save();


        return redirect()->route('products.index',
            $id)->with('flash_message',
            'Product Updated');


    }


    public function destroy($id)
    {
        //
        $product = Product::findOrFail($id);
        $product->delete();

        return redirect()->route('products.index')
            ->with('flash_message',
                'Product Deleted.');
    }


}

<?php

namespace App\Http\Controllers;

use App\Brand;
use Illuminate\Http\Request;

class BrandController extends Controller
{


    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        //
        $brands = Brand::orderby('name', 'asc')->get();

        return view('brands.index', compact('brands'));

    }


    public function create()
    {
        //
        return view('brands.create');

    }


    public function store(Request $request)
    {
        //
        $this->validate($request, [
            'name'=>'required|max:120',
            'status'=>'required',
        ]);

        $brand = new Brand();

        $brand->name = $request->input('name');

        $brand->status = $request->input('status');

        $brand->user_id = auth()->user()->id;

        $brand->save();


        return redirect()->route('brands.index')
            ->with('flash_message',
                'Brand Created');


    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        //
        $brand = Brand::findOrFail($id);

        return view('brands.edit', compact('brand'));
    }


    public function update(Request $request, $id)
    {
        //
        $brand = Brand::findOrFail($id);
        $this->validate($request, [
            'name'=>'required|max:120',
            'status'=>'required',
        ]);


        $brand->name = $request->input('name');

        $brand->status = $request->input('status');

        $brand->user_id = auth()->user()->id;

        $brand->save();


        return redirect()->route('brands.index',
            $id)->with('flash_message',
            'Brand Updated');


    }


    public function destroy($id)
    {
        //
        $brand = Brand::findOrFail($id);
        $brand->delete();

        return redirect()->route('brands.index')
            ->with('flash_message',
                'Brand Deleted.');
    }
}

<?php

namespace App\Http\Controllers;


use App\Brand;
use App\Processor;
use App\Product;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $products = Product::orderby('id', 'desc')->where('status', '=', 1)->count();
        $processors = Processor::orderby('id', 'desc')->where('status', '=', 1)->count();
        $brands = Brand::orderby('id', 'desc')->where('status', '=', 1)->count();


        return view('test-home', compact('products', 'brands', 'processors'));
    }


    public function unverified()
    {
        return view('errors.unverified');
    }



    public function restoreproduct($id)
    {
        //
        $product = Product::where('id',$id)->withTrashed()->first();

         $product->restore();

        return redirect()->route('products.index')
            ->with('flash_message',
                'Product Restored.');
    }

}

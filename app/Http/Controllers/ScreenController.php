<?php

namespace App\Http\Controllers;

use App\Screen;
use Illuminate\Http\Request;

class ScreenController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        //
        $screens = Screen::orderby('name', 'asc')->get();

        return view('screens.index', compact('screens'));

    }


    public function create()
    {
        //
        return view('screens.create');

    }


    public function store(Request $request)
    {
        //
        $this->validate($request, [
            'name'=>'required|max:120',
            'from' => 'required|numeric|between:0.00,999.99',
            'to' => 'required|numeric|between:0.00,999.99|gt:from',
            'status'=>'required',
        ]);

        $screen = new Screen();

        $screen->name = $request->input('name');

        $screen->status = $request->input('status');
        $screen->from = $request->input('from');
        $screen->to = $request->input('to');

        $screen->user_id = auth()->user()->id;

        $screen->save();


        return redirect()->route('screens.index')
            ->with('flash_message',
                'Screen Created');


    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        //
        $screen = Screen::findOrFail($id);

        return view('screens.edit', compact('screen'));
    }


    public function update(Request $request, $id)
    {
        //
        $screen = Screen::findOrFail($id);
        $this->validate($request, [
            'name'=>'required|max:120',
            'from' => 'required|numeric|between:0.00,999.99',
            'to' => 'required|numeric|between:0.00,999.99|gt:from',
            'status'=>'required',
        ]);
        $screen->name = $request->input('name');

        $screen->status = $request->input('status');
        $screen->from = $request->input('from');
        $screen->to = $request->input('to');
        $screen->user_id = auth()->user()->id;

        $screen->save();


        return redirect()->route('screens.index',
            $id)->with('flash_message',
            'Screen Updated');


    }


    public function destroy($id)
    {
        //
        $screen = Screen::findOrFail($id);
        $screen->delete();

        return redirect()->route('screens.index')
            ->with('flash_message',
                'Screen Deleted.');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');


            $table->unsignedInteger('brand_id');
            $table->foreign('brand_id')->references('id')->on('brands');

            $table->unsignedInteger('processor_id');
            $table->foreign('processor_id')->references('id')->on('processors');

            $table->unsignedInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');



            $table->string('name')->nullable();
            $table->double('price', 8, 2)->default(0);
            $table->double('screen_size', 8, 2)->default(0);
            $table->integer('availability')->default(1);
            $table->integer('touch_screen')->default(1);
            $table->integer('status')->default(1);

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
